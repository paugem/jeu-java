package com.cda.pfr.cdaLegacy;

import com.badlogic.gdx.Game;

/** {@link com.badlogic.gdx.ApplicationListener} implementation shared by all platforms. */
public class TheGame extends Game {
	@Override
	public void create() {
		setScreen(new FirstScreen());
	}
}